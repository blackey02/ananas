Ananas
====================

Ananas... Why?
---------------------
![pineapple](https://googledrive.com/host/0B3Wl7H-4O2W9RlhSOURWZkVGbG8) Swedish for pineapple, and pineapples are okay in my book.

Summary
---------------------
After all the discussions around the new SDK one issue keeps coming up, the inevitable deprecation of the Activex control. Ananas was born to solve this problem and acts as a javascript SDK or shim between javascript and the C++ SDK itself via a cross browser supported plug-in. 

[Demo Video](https://bitbucket.org/blackey02/ananas/downloads/AnanasDemo.mp4)

Building
---------------------
0. Install the Pelco SDK
1. 'git clone https://bitbucket.org/blackey02/ananas.git'
2. Install [CMake](http://www.cmake.org/cmake/help/install.html)
3. Install [Typescript](http://www.typescriptlang.org)
4. Open 'cmd.exe' and navigate to ananas/ and run 'prep2010.cmd' to create a VC2010 solution
5. Open 'cmd.exe' and navigate to ananas/build/projects/Ananas/gen/ then run 'tsc --out script.js main.ts sdk-wrapper.ts'
6. Navigate to ananas/build/projects/Ananas/ and build Ananas.sln
7. Open 'cmd.exe' and navigate to ananas/build/bin/Ananas/Debug/ then run 'regsvr32 npAnanas.dll'
8. Navigate to ananas/build/projects/Ananas/gen/ and open 'FBControl.htm' with your browser of choice

Acknowledgements
---------------------
* [Pelco](http://pdn.pelco.com)
* [FireBreath](http://www.firebreath.org/display/documentation/FireBreath+Home)
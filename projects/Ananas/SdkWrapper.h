#ifndef _SdkWrapper_h
#define _SdkWrapper_h

#include "APITypes.h"
#include "Credentials.h"
#include "PelcoSDK\SystemCollection.h"
#include "PelcoSystem.h"

class SdkWrapper {
public:
	SdkWrapper();
	virtual ~SdkWrapper();

	bool AddSystem(Credentials&);
	bool AddEdgeDevice(Credentials&);
	FB::VariantList GetSystems();
	bool Login(const std::string&, Credentials&);
	bool Logout(const std::string&);
	FB::VariantList GetCameras(const std::string&);
	bool Connect(void*, const std::string&, const std::string&);
	bool Play(const std::string&, float);
	bool Stop(const std::string&);

private:
	typedef boost::shared_ptr<PelcoSystem> PelcoSystemPtr;
	typedef std::map<std::string,PelcoSystemPtr> SystemMap;
	typedef std::map<std::string,PelcoSystemPtr>::iterator SystemMapIter;

	PelcoSystemPtr FindSystem(const std::string&);
	PelcoSystemPtr FindEdgeSystem();

private:
	PelcoSDK::SystemCollection _systemCollection;
	SystemMap _systemMap;
};

/*
todo, SystemMap will need to hold a custom object instead of simply the PelcoSDK::System
because when we create a stream we need to keep a reference to the stream object and we have to put it somewhere.
*/

#endif // _SdkWrapper_h
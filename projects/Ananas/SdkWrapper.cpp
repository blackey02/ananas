#include "SdkWrapper.h"
#include "PelcoSDK\PelcoSDK.h"
#include "PelcoSDK\System.h"
#include "PelcoSDK\Exception.h"
#include "boost\thread\thread.hpp"

typedef boost::shared_ptr<PelcoSystem> PelcoSystemPtr;

const std::string EdgeAlias = "AnanasEdge";

FB::VariantMap CreateError();
FB::VariantMap CreateError(PelcoSDK::Exception);
FB::VariantMap CreateError(const std::exception&);

SdkWrapper::SdkWrapper() {
	HRESULT hr = ::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	PelcoSDK::Startup();

	try {
		_systemCollection.Add("pelcoedgedevices://?alias=" + EdgeAlias);
	}
	catch(PelcoSDK::Exception) {}

	for(_systemCollection.Reset(); _systemCollection.MoveNext();) {
		PelcoSDK::System *system = new PelcoSDK::System(_systemCollection.Current());
		_systemMap[system->GetUUID()] = PelcoSystemPtr(new PelcoSystem(system));
	}
}

SdkWrapper::~SdkWrapper() {
	_systemMap.clear();
	PelcoSDK::Shutdown();
	::CoUninitialize();
}

bool SdkWrapper::AddSystem(Credentials &credentials) {
	bool success = true;
	std::string scheme = credentials.CreateScheme(false);

	try {
		_systemCollection.Add(scheme);
		PelcoSDK::System *newSystem = new PelcoSDK::System(scheme);
		_systemMap[newSystem->GetUUID()] = PelcoSystemPtr(new PelcoSystem(newSystem));
	}
	catch(PelcoSDK::Exception e) {
		success = false;
	}
	return success;
}

bool SdkWrapper::AddEdgeDevice(Credentials &credentials) {
	bool success = true;
	std::string scheme = credentials.CreateScheme(true, EdgeAlias);

	try {
		PelcoSystemPtr edgeSystem = FindEdgeSystem();
		edgeSystem->AddDevice(scheme);
	}
	catch(PelcoSDK::Exception) {
		success = false;
	}
	return success;
}

FB::VariantList SdkWrapper::GetCameras(const std::string &uuid) {
	FB::VariantList cameras;

	try {
		PelcoSystemPtr system = FindSystem(uuid);
		if(!system)
			throw std::runtime_error("Unable to find system uuid specified");
		cameras = system->GetCameras();
	}
	catch(PelcoSDK::Exception e) {
		cameras.push_back(CreateError(e));
	}
	catch(const std::exception &e) {
		cameras.push_back(CreateError(e));
	}
	catch(...) {
		cameras.push_back(CreateError());
	}
	return cameras;
}

FB::VariantList SdkWrapper::GetSystems() {
	FB::VariantList systems;

	for(SystemMapIter it = _systemMap.begin(); it != _systemMap.end(); it++) {
		PelcoSystemPtr system = it->second;
		FB::VariantMap map;
		map["alias"] = FB::variant(system->GetAlias());
		map["displayName"] = FB::variant(system->GetDisplayName());
		map["ip"] = FB::variant(system->GetIp());
		map["uuid"] = FB::variant(system->GetUuid());
		map["port"] = FB::variant(system->GetPort());
		systems.push_back(map);
	}
	return systems;
}

bool SdkWrapper::Login(const std::string &uuid, Credentials &credentials) {
	bool success = true;
	try {
		PelcoSystemPtr system = FindSystem(uuid);
		if(!system)
			throw std::runtime_error("Unable to find system uuid specified");
		system->Login(credentials.Username,credentials.Password);
	}
	catch(...) {
		success = false;
	}
	return success;
}

bool SdkWrapper::Logout(const std::string &uuid) {
	bool success = true;
	try {
		PelcoSystemPtr system = FindSystem(uuid);
		if(!system)
			throw std::runtime_error("Unable to find system uuid specified");
		system->Login("SdkWrapper", "Logout");
	}
	catch(PelcoSDK::Exception e) {
		success = e.Error() == PelcoSDK::InvalidCredentials;
	}
	catch(...) {
		success = false;
	}
	return success;
}

PelcoSystemPtr SdkWrapper::FindSystem(const std::string &uuid) {
	PelcoSystemPtr system;
	bool found = _systemMap.find(uuid) != _systemMap.end();
	if(found)
		system = _systemMap[uuid];
	return system;
}

PelcoSystemPtr SdkWrapper::FindEdgeSystem() {
	PelcoSystemPtr system;
	for(SystemMapIter it = _systemMap.begin(); it != _systemMap.end(); it++) {
		if(std::string(it->second->GetAlias()) == EdgeAlias) {
			system = it->second;
			break;
		}
	}
	return system;
}

bool SdkWrapper::Connect(void *hwnd, const std::string &systemUuid, const std::string &cameraUuid) {
	bool success = false;
	try {
		PelcoSystemPtr system = FindSystem(systemUuid);
		if(!system)
			throw std::runtime_error("Unable to find system uuid specified");
		success = system->Connect(hwnd, cameraUuid);
	}
	catch(...) {}
	return success;
}

bool SdkWrapper::Play(const std::string &systemUuid, float speed) {
	bool success = false;
	try {
		PelcoSystemPtr system = FindSystem(systemUuid);
		if(!system)
			throw std::runtime_error("Unable to find system uuid specified");
		success = system->Play(speed);
	}
	catch(...) {
		success = false;
	}
	return success;
}

bool SdkWrapper::Stop(const std::string &systemUuid) {
	bool success = false;
	try {
		PelcoSystemPtr system = FindSystem(systemUuid);
		if(!system)
			throw std::runtime_error("Unable to find system uuid specified");
		system->Stop();
		success = true;
	}
	catch(...) {}
	return success;
}

//////////

FB::VariantMap CreateError() {
	FB::VariantMap error;
	error["result"] = FB::variant("error");
	error["errorCode"] = FB::variant(0);
	error["message"] = FB::variant("General exception");
	return error;
}

FB::VariantMap CreateError(const std::exception &e) {
	FB::VariantMap error;
	error["result"] = FB::variant("error");
	error["errorCode"] = FB::variant(0);
	error["message"] = FB::variant(e.what());
	return error;
}

FB::VariantMap CreateError(PelcoSDK::Exception e) {
	FB::VariantMap error;
	error["result"] = FB::variant("error");
	error["errorCode"] = FB::variant((int)e.Error());
	error["message"] = FB::variant(e.Message().c_str());
	return error;
}
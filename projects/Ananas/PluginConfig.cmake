#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for Ananas
#
#\**********************************************************/

set(PLUGIN_NAME "Ananas")
set(PLUGIN_PREFIX "ANA")
set(COMPANY_NAME "Pelco")

# ActiveX constants:
set(FBTYPELIB_NAME AnanasLib)
set(FBTYPELIB_DESC "Ananas 1.0 Type Library")
set(IFBControl_DESC "Ananas Control Interface")
set(FBControl_DESC "Ananas Control Class")
set(IFBComJavascriptObject_DESC "Ananas IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "Ananas ComJavascriptObject Class")
set(IFBComEventSource_DESC "Ananas IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID d8305c88-bd81-5f20-9365-9ced9ea3cf4b)
set(IFBControl_GUID 42831af1-d6ea-5654-8c90-4ece32baa98e)
set(FBControl_GUID c45c0cfe-9b25-55bd-88de-bb4f8cf4dcaf)
set(IFBComJavascriptObject_GUID 7f77cac9-849d-510a-a5db-5ed750d6685b)
set(FBComJavascriptObject_GUID 3da585c7-89cc-552a-8fb4-55c089e54122)
set(IFBComEventSource_GUID 6248df19-98d6-5c57-9fe1-ad3411e49736)
if ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID eb27ec1a-9438-5da3-9909-e5c0d9607916)
else ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID c6ebb985-6ceb-58b9-a7fb-cb890e70d282)
endif ( FB_PLATFORM_ARCH_32 )

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "Pelco.Ananas")
if ( FB_PLATFORM_ARCH_32 )
    set(MOZILLA_PLUGINID "pelco.com/Ananas")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
    set(MOZILLA_PLUGINID "pelco.com/Ananas_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )

# strings
set(FBSTRING_CompanyName "Pelco")
set(FBSTRING_PluginDescription "Pelco SDK Media Plugin")
set(FBSTRING_PLUGIN_VERSION "1.0.0.0")
set(FBSTRING_LegalCopyright "Copyright 2014 Pelco")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}")
set(FBSTRING_ProductName "Ananas")
set(FBSTRING_FileExtents "")
if ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "Ananas")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "Ananas_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )
set(FBSTRING_MIMEType "application/x-ananas")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

#set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 1)
set(FBMAC_USE_COCOA 1)
set(FBMAC_USE_COREGRAPHICS 1)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)

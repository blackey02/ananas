#include <sstream>
#include "Credentials.h"

Credentials::Credentials(const FB::VariantMap &credentials) {
	Ip = credentials.find("ip") != credentials.end() ? credentials.at("ip").convert_cast<std::string>() : "";
	Port = credentials.find("port") != credentials.end() ? credentials.at("port").convert_cast<unsigned short>() : 0;
	Username = credentials.find("username") != credentials.end() ? credentials.at("username").convert_cast<std::string>() : "";
	Password = credentials.find("password") != credentials.end() ? credentials.at("password").convert_cast<std::string>() : "";
}

Credentials::~Credentials() {
}

std::string Credentials::CreateScheme(bool isEdge, std::string alias) const {
	std::string provider = isEdge ? "pelcoedgedevices" : "pelcosystem";
	std::ostringstream scheme;
	scheme << Username << ":" << Password << "@" << provider << "://";
	scheme << Ip << ":" << Port;

	if(alias == "")
		scheme << "?alias=" << provider << ": " << Ip << ":" << Port;
	else
		scheme << "?alias=" << alias;
	
	return scheme.str();
}
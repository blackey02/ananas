#include "PelcoSystem.h"
#include "PelcoSDK\Exception.h"
#include "PelcoSDK\Stream.h"
#include "PelcoSDK\Display.h"

typedef boost::shared_ptr<PelcoSDK::Device> PelcoSdkDevicePtr;

PelcoSystem::PelcoSystem(PelcoSDK::System *system) {
	_system = PelcoSdkSystemPtr(system);
	_stream = NULL;
}

PelcoSystem::~PelcoSystem() {
	//todo: delete _stream;
}

FB::VariantList PelcoSystem::GetCameras() {
	FB::VariantList cameras;

	PelcoSDK::DeviceCollection devices = _system->GetDeviceCollection();
	for(devices.Reset(); devices.MoveNext();) {
		PelcoSDK::Device device = devices.Current();
		if(device.GetDeviceType() == PelcoSDK::CAMERA) {
			FB::VariantMap map;
			map["friendlyName"] = FB::variant(device.GetFriendlyName().c_str());
			map["ip"] = FB::variant(device.GetIp().c_str());
			map["port"] = FB::variant(device.GetPort());
			map["modelName"] = FB::variant(device.GetModelName().c_str());
			map["modelNumber"] = FB::variant(device.GetModelNumber().c_str());
			map["uuid"] = FB::variant(device.GetUDN().c_str());
			map["isOnline"] = FB::variant(device.IsOnline());
			cameras.push_back(map);
		}
	}
	return cameras;
}

std::string PelcoSystem::GetAlias() {
	return _system->GetAlias();
}

std::string PelcoSystem::GetDisplayName() {
	return _system->GetDisplayName();
}

std::string PelcoSystem::GetIp() {
	return _system->GetIp();
}

unsigned short PelcoSystem::GetPort() {
	unsigned short port = 0;
	try {
		port = _system->GetPort();
	}
	catch(PelcoSDK::Exception) {}
	return port;
}

std::string PelcoSystem::GetUuid() {
	return _system->GetUUID();
}

void PelcoSystem::AddDevice(const std::string &scheme) {
	_system->GetDeviceCollection().Add(scheme);
}

bool PelcoSystem::Login(const std::string &username, const std::string &passwd) {
	return _system->Login(PelcoSDK::PString(username), PelcoSDK::PString(passwd));
}

bool PelcoSystem::IsPlaying() {
	return _isPlaying;
}

bool PelcoSystem::Connect(void *hwnd, const std::string &cameraUuid) {
	bool success = false;
	try {
		Stop();
		
		PelcoSDK::Device device = FindDevice(cameraUuid);
		PelcoSDK::Camera camera(device);
		PelcoSDK::Display display((HWND)hwnd);
		
		// Leaks memory, never deleted, figure out why when delete is called a crash occurs.
		_stream = new PelcoSDK::Stream(camera);

		display.Show(*_stream);
		success = true;
	}
	catch(...) {}
	return success;
}

bool PelcoSystem::Play(float speed) {
	bool success = false;
	try {
		if(_stream) {
			_stream->Play(speed);
			success = true;
		}
	}
	catch(...) {}
	return success;
}

bool PelcoSystem::Stop() {
	bool success = false;
	try {
		if(_stream) {
			_stream->Stop();
			//todo delete _stream;
			_stream = NULL;
			success = true;
		}
	}
	catch(...) {}
	return success;
}

PelcoSDK::Device PelcoSystem::FindDevice(const std::string &cameraUuid) {
	PelcoSDK::DeviceCollection devices = _system->GetDeviceCollection();
	for(devices.Reset(); devices.MoveNext();) {
		PelcoSDK::Device device = devices.Current();
		if(std::string(device.GetUDN()) == cameraUuid)
			return device;
	}
	throw std::runtime_error("unable to find the camera uuid specified");
}
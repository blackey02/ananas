#include <iostream>
#include "APITypes.h"

#ifndef _Credentials_h
#define _Credentials_h

class Credentials {
public:
	Credentials(const FB::VariantMap&);
	virtual ~Credentials();

	std::string CreateScheme(bool isEdge, std::string alias = "") const;

	std::string Ip;
	unsigned short Port;
	std::string Username;
	std::string Password;
};

#endif // _Credentials_h
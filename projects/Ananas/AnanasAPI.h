/**********************************************************\

  Auto-generated AnanasAPI.h

\**********************************************************/

#include <string>
#include <sstream>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "Ananas.h"
#include "Credentials.h"

#ifndef H_AnanasAPI
#define H_AnanasAPI

class SdkWrapper;

class AnanasAPI : public FB::JSAPIAuto
{
public:
    ////////////////////////////////////////////////////////////////////////////
    /// @fn AnanasAPI::AnanasAPI(const AnanasPtr& plugin, const FB::BrowserHostPtr host)
    ///
    /// @brief  Constructor for your JSAPI object.
    ///         You should register your methods, properties, and events
    ///         that should be accessible to Javascript from here.
    ///
    /// @see FB::JSAPIAuto::registerMethod
    /// @see FB::JSAPIAuto::registerProperty
    /// @see FB::JSAPIAuto::registerEvent
    ////////////////////////////////////////////////////////////////////////////
    AnanasAPI(const AnanasPtr &plugin, const FB::BrowserHostPtr &host);

    ///////////////////////////////////////////////////////////////////////////////
    /// @fn AnanasAPI::~AnanasAPI()
    ///
    /// @brief  Destructor.  Remember that this object will not be released until
    ///         the browser is done with it; this will almost definitely be after
    ///         the plugin is released.
    ///////////////////////////////////////////////////////////////////////////////
    virtual ~AnanasAPI();

    AnanasPtr GetPlugin();

public:
	bool AddSystem(const FB::VariantMap&);
	void AddSystemAsync(const FB::VariantMap&, FB::JSObjectPtr&);
	bool AddEdgeDevice(const FB::VariantMap&);
	void AddEdgeDeviceAsync(const FB::VariantMap&, FB::JSObjectPtr&);
	FB::VariantList GetSystems();
	bool Login(const std::string&, const FB::VariantMap&);
	bool Logout(const std::string&);
	FB::VariantList GetCameras(const std::string&);
	bool Connect(const std::string&, const std::string&);
	void ConnectAsync(const std::string&, const std::string&, FB::JSObjectPtr&);
	bool Play(const std::string&, float);
	bool Stop(const std::string&);

private:
	void AddSystemAsyncImpl(const FB::VariantMap&, FB::JSObjectPtr&);
	void AddEdgeDeviceAsyncImpl(const FB::VariantMap&, FB::JSObjectPtr&);
	void ConnectAsyncImpl(const std::string&, const std::string&, FB::JSObjectPtr&);

private:
	typedef boost::shared_ptr<SdkWrapper> SdkWrapperPtr;

    AnanasWeakPtr _plugin;
    FB::BrowserHostPtr _host;
	SdkWrapperPtr _sdkWrapper;
};

#endif // H_AnanasAPI


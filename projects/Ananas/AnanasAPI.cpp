#include "JSObject.h"
#include "APITypes.h"
#include "DOM/Document.h"
#include "global/config.h"
#include "AnanasAPI.h"
#include "SdkWrapper.h"

AnanasAPI::AnanasAPI(const AnanasPtr &plugin, const FB::BrowserHostPtr &host) {
	_plugin = plugin;
	_host = host;
	_sdkWrapper = SdkWrapperPtr(new SdkWrapper());

	registerMethod("addSystem", make_method(this, &AnanasAPI::AddSystem));
	registerMethod("addSystemAsync", make_method(this, &AnanasAPI::AddSystemAsync));
	registerMethod("addEdgeDevice", make_method(this, &AnanasAPI::AddEdgeDevice));
	registerMethod("addEdgeDeviceAsync", make_method(this, &AnanasAPI::AddEdgeDeviceAsync));
	registerMethod("getSystems", make_method(this, &AnanasAPI::GetSystems));
	registerMethod("login", make_method(this, &AnanasAPI::Login));
	registerMethod("logout", make_method(this, &AnanasAPI::Logout));
    registerMethod("getCameras", make_method(this, &AnanasAPI::GetCameras));
	registerMethod("connect", make_method(this, &AnanasAPI::Connect));
	registerMethod("connectAsync", make_method(this, &AnanasAPI::ConnectAsync));
	registerMethod("play", make_method(this, &AnanasAPI::Play));
	registerMethod("stop", make_method(this, &AnanasAPI::Stop));
}

AnanasAPI::~AnanasAPI() {
}

AnanasPtr AnanasAPI::GetPlugin() {
    AnanasPtr plugin(_plugin.lock());
    if (!plugin)
        throw FB::script_error("The plugin is invalid");
    return plugin;
}

FB::VariantList AnanasAPI::GetCameras(const std::string &uuid) {
	return _sdkWrapper->GetCameras(uuid);
}

FB::VariantList AnanasAPI::GetSystems() {
	return _sdkWrapper->GetSystems();
}

bool AnanasAPI::AddSystem(const FB::VariantMap &credentials) {
	Credentials cred(credentials);
	return _sdkWrapper->AddSystem(cred);
}

void AnanasAPI::AddSystemAsync(const FB::VariantMap &credentials, FB::JSObjectPtr &callback) {
	boost::thread t(boost::bind(&AnanasAPI::AddSystemAsyncImpl, this, credentials, callback));
}

void AnanasAPI::AddSystemAsyncImpl(const FB::VariantMap &credentials, FB::JSObjectPtr &callback) {
	HRESULT hr = ::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	bool success = AddSystem(credentials);
	callback->InvokeAsync("", FB::variant_list_of(FB::variant(success)));
	::CoUninitialize();
}

bool AnanasAPI::AddEdgeDevice(const FB::VariantMap &credentials) {
	Credentials cred(credentials);
	return _sdkWrapper->AddEdgeDevice(cred);
}

void AnanasAPI::AddEdgeDeviceAsync(const FB::VariantMap &credentials, FB::JSObjectPtr &callback) {
	boost::thread t(boost::bind(&AnanasAPI::AddEdgeDeviceAsyncImpl, this, credentials, callback));
}

void AnanasAPI::AddEdgeDeviceAsyncImpl(const FB::VariantMap &credentials, FB::JSObjectPtr &callback) {
	HRESULT hr = ::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	bool success = AddEdgeDevice(credentials);
	callback->InvokeAsync("", FB::variant_list_of(FB::variant(success)));
	::CoUninitialize();
}

bool AnanasAPI::Login(const std::string &uuid, const FB::VariantMap &credentials) {
	Credentials cred(credentials);
	return _sdkWrapper->Login(uuid, cred);
}

bool AnanasAPI::Logout(const std::string &uuid) {
	return _sdkWrapper->Logout(uuid);
}

bool AnanasAPI::Connect(const std::string &systemUuid, const std::string &cameraUuid) {
	void *window = GetPlugin()->GetWindowHandle();
	return _sdkWrapper->Connect(window, systemUuid, cameraUuid);
}

void AnanasAPI::ConnectAsync(const std::string &systemUuid, const std::string &cameraUuid, FB::JSObjectPtr &callback) {
	boost::thread t(boost::bind(&AnanasAPI::ConnectAsyncImpl, this, systemUuid, cameraUuid, callback));
}

void AnanasAPI::ConnectAsyncImpl(const std::string &systemUuid, const std::string &cameraUuid, FB::JSObjectPtr &callback) {
	HRESULT hr = ::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	bool success = Connect(systemUuid, cameraUuid);
	callback->InvokeAsync("", FB::variant_list_of(FB::variant(success)));
	::CoUninitialize();
}

bool AnanasAPI::Play(const std::string &systemUuid, float speed) {
	return _sdkWrapper->Play(systemUuid, speed);
}

bool AnanasAPI::Stop(const std::string &systemUuid) {
	return _sdkWrapper->Stop(systemUuid);
}

#include "PelcoSDK\System.h"
#include "PelcoSDK\Camera.h"
#include "PelcoSDK\Display.h"
#include "APITypes.h"

#ifndef _PelcoSystem_h
#define _PelcoSystem_h

class PelcoSystem {
public:
	PelcoSystem(PelcoSDK::System*);
	virtual ~PelcoSystem();

	std::string GetAlias();
	std::string GetDisplayName();
	std::string GetIp();
	unsigned short GetPort();
	std::string GetUuid();
	FB::VariantList GetCameras();
	void AddDevice(const std::string&);
	bool Login(const std::string&, const std::string&);
	bool Connect(void*, const std::string&);
	bool Play(float);
	bool Stop();

public:
	bool IsPlaying();

private:
	PelcoSDK::Device FindDevice(const std::string&);

private:
	typedef boost::shared_ptr<PelcoSDK::System> PelcoSdkSystemPtr;

	PelcoSdkSystemPtr _system;
	PelcoSDK::Stream *_stream;
	bool _isPlaying;
};

#endif // _PelcoSystem_h
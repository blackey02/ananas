declare var selectedSystem;
declare var selectedCamera;

class Main {
    private sdkWrapper: SDKWrapper;
    private plugin: any;
    private usernameInput: any;
    private passwdInput: any;
    private isEdgeCheckBox: any;
    private ipInput: any;
    private portInput: any;
    private addSystemButton: any;
    private getSystemsButton: any;
    private getCamerasButton: any;
    private systemsContent: any;
    private camerasContent: any;
    
    constructor(pluginId: string) {
        this.plugin = document.getElementById(pluginId);
        this.sdkWrapper = new SDKWrapper(this.plugin);
        this.usernameInput = document.getElementById('username');
        this.passwdInput = document.getElementById('passwd');
        this.isEdgeCheckBox = document.getElementById('is-edge');
        this.isEdgeCheckBox.onclick = () => {
            this.isEdgeCheckBoxClicked.apply(this, arguments);
        }
        this.ipInput = document.getElementById('system-ip');
        this.portInput = document.getElementById('system-port');
        this.addSystemButton = document.getElementById('system-add');
        this.addSystemButton.onclick = () => {
            this.addSystemButtonClicked.apply(this, arguments);
        }
        this.getSystemsButton = document.getElementById('system-get');
        this.getSystemsButton.onclick = () => {
            this.getSystemsButtonClicked.apply(this, arguments);
        }
        this.getCamerasButton = document.getElementById('cameras-get');
        this.getCamerasButton.onclick = () => {
            this.getCamerasButtonClicked.apply(this, arguments);
        }
        this.systemsContent = document.getElementById('systems');
        this.camerasContent = document.getElementById('cameras');
    }
    
    pluginLoaded() {
        console.log("Plugin Loaded!");
    }
    
    isEdgeCheckBoxClicked() {
        this.portInput.value = this.isEdgeCheckBox.checked ? "80" : "60001";
    }
    
    addSystemButtonClicked() {
        this.disableUi();
        var credentials = new Credentials();
        credentials.ip = this.ipInput.value;
        credentials.port = this.portInput.value;
        credentials.username = this.usernameInput.value;
        credentials.password = this.passwdInput.value;
    
        var addSystemFunc = (success) => {
            this.disableUi(false);
            console.log(success);
        }
        
        if(this.isEdgeCheckBox.checked)
            this.sdkWrapper.addEdgeDeviceAsync(credentials, addSystemFunc);
        else
            this.sdkWrapper.addSystemAsync(credentials, addSystemFunc);
    }
    
    getSystemsButtonClicked() {
        this.startWorking(() => {
            selectedCamera = null;
            var systems = this.sdkWrapper.getSystems();
            console.log(systems);
            this.displaySystems(systems);
        });
    }
    
    getCamerasButtonClicked() {
        this.startWorking(() => {
            selectedCamera = null;
            var cameras = this.sdkWrapper.getCameras(selectedSystem);
            console.log(cameras);
            this.displayCameras(cameras);
        });
    }
    
    loginClicked() {
        this.startWorking(() => {
            var credentials = new Credentials();
            credentials.username = this.usernameInput.value;
            credentials.password = this.passwdInput.value;
            
            var success = this.sdkWrapper.login(selectedSystem, credentials);
            console.log(success);
        });
    }
    
    startWorking(callback: any = null, delay: number = 500) {
        this.disableUi();
        var callbackInternal = () => { callback(); this.disableUi(false); }
        window.setTimeout(callbackInternal, delay);
    }
    
    disableUi(disable: boolean = true) {
        this.usernameInput.disabled = disable;
        this.passwdInput.disabled = disable;
        this.isEdgeCheckBox.disabled = disable;
        this.ipInput.disabled = disable;
        this.portInput.disabled = disable;
        this.addSystemButton.disabled = disable;
        this.getSystemsButton.disabled = disable;
        this.getCamerasButton.disabled = disable;
    }
    
    displaySystems(systems: any[]) {
        var items = '';
        for(var i = 0; i < systems.length; i++) {
            var system = systems[i];
            var item = '<div>';
            item += '<input type="radio" name="systems" onclick="selectedSystem = \'' + system.uuid + '\';" />';
            item += ' - ' + system.displayName;
            item += '</div>';
            items += item;
        }
        this.systemsContent.innerHTML = items;
    }
    
    displayCameras(cameras: any[]) {
        var items = '';
        for(var i = 0; i < cameras.length; i++) {
            var camera = cameras[i];
            var item = '<div>';
            if(camera.errorCode) {
                item += '<input type="radio" name="cameras" onclick="selectedCamera = null"; /> Error: (' + camera.errorCode + ') ' + camera.message; 
                if(camera.errorCode == 22) {
                    item += ' <input type="button" value="Login" onclick="main.loginClicked();" />';
                }
            }
            else {
                item += '<input type="radio" name="cameras" onclick="selectedCamera = \'' + camera.uuid + '\';" />';
                item += ' - ' + camera.friendlyName;
            }
            item += '</div>';
            items += item;
        }
        this.camerasContent.innerHTML = items;
    }
    
    stop() {
        console.log(this.sdkWrapper.stop(selectedSystem));
    }
    
    play() {
        this.stop();
        this.sdkWrapper.connectAsync(selectedSystem, selectedCamera, (success) => {
            console.log(success);
            if(success)
                console.log(this.sdkWrapper.play(selectedSystem, 1));
        });
    }
    
    rewind() {
        console.log(this.sdkWrapper.play(selectedSystem, -1));
    }
}





















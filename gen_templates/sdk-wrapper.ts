
interface ICredentials {
    ip?: string;
    port?: number;
    username: string;
    password: string;
}

class Credentials implements ICredentials {
    ip: string;
    port: number;
    username: string;
    password: string;
}

class SDKWrapper {
    private plugin: any;
    
    constructor(plugin: any) {
        this.plugin = plugin;
    }
    
    addSystem(credentials: ICredentials):boolean {
        return this.plugin.addSystem(credentials);
    }
    
    addSystemAsync(credentials: ICredentials, callback: any) {
        this.plugin.addSystemAsync(credentials, callback);
    }
    
    addEdgeDevice(credentials: ICredentials):boolean {
        return this.plugin.addEdgeDevice(credentials);
    }
    
    addEdgeDeviceAsync(credentials: ICredentials, callback: any) {
        this.plugin.addEdgeDeviceAsync(credentials, callback);
    }
    
    getSystems() {
        return this.plugin.getSystems();
    }
    
    login(uuid: string, credentials: ICredentials):boolean {
        return this.plugin.login(uuid, credentials);
    }
    
    logout(uuid: string):boolean {
        return this.plugin.logout(uuid);
    }
    
    getCameras(uuid: string) {
        return this.plugin.getCameras(uuid);
    }
    
    connect(systemUuid: string, cameraUuid: string) {
        return this.plugin.connect(systemUuid, cameraUuid);
    }
    
    connectAsync(systemUuid: string, cameraUuid: string, callback: any) {
        this.plugin.connectAsync(systemUuid, cameraUuid, callback);
    }
    
    play(systemUuid: string, speed: number) {
        return this.plugin.play(systemUuid, speed);
    }
    
    stop(systemUuid: string) {
        return this.plugin.stop(systemUuid);
    }
}

























